import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, map } from 'rxjs';
import { ApiServices } from '../Service/api-services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = true;
  admin = false;
  email: any;
  password: any;

  type: any = 'user';
  constructor(private apiService: ApiServices,private router : Router) { }

  ngOnInit(): void {
  }

  loginDetails() {
    this.user = !this.user;
    this.admin = !this.admin;
  }
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

  login(){
    let data={
      email: this.email,
      password: this.password
    }
    this.apiService.login(data,this.type)
      .subscribe(
        resp => console.log(resp),
        async err => {
          if (err.error.text == 'Login Success') {
            alert("LoggedIn Successfully");
            localStorage.setItem('loggedinUser',this.email);
            localStorage.setItem('loggedInUserType',this.type);
            this.router.navigate(['/Questions']);
            await this.delay(2000);
            window.location.reload();

          } else {
            alert(err.error.text);
          }
        }
      );
    
  }


}
