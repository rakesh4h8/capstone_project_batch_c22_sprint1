package com.gl.capstoneproject.doconnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
@EnableDiscoveryClient
@SpringBootApplication
public class DoconnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoconnectApplication.class, args);
	}
}	